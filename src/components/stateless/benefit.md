- No class needed
- Avoid this keyword
- Enforce best pratices
- High signal-to-noise ratio
- Enhanced code completion / intellisense
- Bloated components are obvious
- Easy to understand
- Easy to test
- Performance (no state, lifecycle => React no need check)


=>> User whenever possible

Class Component
    + State
    + Refs
    + Lifecycle methods
    + Child functions (for performance)

Stateless Component
    + Everywhere else

Other way to create component
    + Object.create
    + Mixins
    + Parasitic Components
    + Stamplt

Container vs Presentation Components
    + Container
        + Little to no markup
        + Statefull
        + Pass data and action down
        + Knows about Redux
    + Presentation (most component)
        + Nearly all markup
        + Received data and actions via props
        + Doesnt know about Redux
        + Typically functional components