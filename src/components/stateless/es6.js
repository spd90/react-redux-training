import React from 'react';

const ES6Stateless = (props) => {
    const handleClick = (event) => {
        alert('Clicking button');
    }
    return (
        <div>
            This is stateless component
            <button type="button" onClick={handleClick}>Click Event With ES6</button>
        </div>
    );
};

export default ES6Stateless;