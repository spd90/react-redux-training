import React, { Component } from 'react';

class ES6Statefull extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        alert('Clicking button');
    }
    render() {
        return (
            <div>
                This is ES6 statefull class(component)
                <button type="button" onClick={this.handleClick}>Click Event With ES6</button>
            </div>
        );
    }
}

export default ES6Statefull;