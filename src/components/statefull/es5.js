import React from 'react';

var ES5StateLess = React.createClass({
    handleClick: function() {
        alert('Clicking button');
    },
    render: function() {
        return (
            <div>This is ES5 react class (component)
                <button type="button" onClick={this.handleClick.bind(this)}>Click Event with ES5 Class</button>
            </div>
        );
    },
});

export default ES5StateLess;