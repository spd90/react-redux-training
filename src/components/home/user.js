import React from 'react';

const User = (props) => {
    return (
        <div className="col-md-12">
            <div className="well well-sm">
                <div className="row">
                    <div className="col-sm-6 col-md-4">
                        <img src={props.user.avatar_url} alt="" className="img-rounded img-responsive" />
                    </div>
                    <div className="col-sm-6 col-md-8">
                        <h4>{props.user.login}</h4>
                        <small>
                          <cite title="Ha Noi - Viet Nam">Ha Noi - Viet Nam 
                            <i className="glyphicon glyphicon-map-marker"></i>
                          </cite>
                        </small>
                        <p>
                        <i className="glyphicon glyphicon-envelope"></i>quangdv190@gmail.com
                        <br />
                        <i className="glyphicon glyphicon-globe"></i><a href={props.user.html_url}>{props.user.html_url}</a>
                        <br />
                        <i className="glyphicon glyphicon-gift"></i>{props.user.created_at}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default User;