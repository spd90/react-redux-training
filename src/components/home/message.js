import React from 'react';
import './message.css';
const Message = (props) => {
    return (
        <div className="col-md-12">
            <h4><span className="label label-lg label-success"><i className="glyphicon glyphicon-refresh gly-spin" aria-hidden="true"></i> {props.message}</span></h4>
        </div>
    );
};

export default Message;