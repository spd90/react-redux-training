import React, { Component } from 'react';
import Message from './message';
import UserCard from './user';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            isFetched: false,
            search: {
                userName: ''
            }
        };
        this.loadUserData = this.loadUserData.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }
    loadUserData(event) {
        event.preventDefault();
        let self = this;
        let userName = this.state.search.userName;
        fetch(`https://api.github.com/users/${userName}`).then((response) => {
            return response.json();
        }).then((json) => {
            self.setState({
                user: json,
                isFetched: true
            });
            return self;
        });
    }
    onInputChange(event) {
        this.setState({
            search: {
                userName: event.target.value
            }
        });
    }
    componentWillMount() {
        console.log('Mounting component');        
    }
    componentDidMount() {
        let self = this;
        setTimeout(function() {
            console.log('fetching data from API: https://api.github.com/users/netsstea');
            fetch('https://api.github.com/users/netsstea').then((response) => {
                return response.json();
            }).then((json) => {
                self.setState({
                    user: json,
                    isFetched: true
                });
                return self;
            });
        }, 3000);
        console.log('Component mounted');
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log('Checking stuff in shouldComponentUpdate');
        console.info('nextProps:' + nextProps);
        console.info('nextState' + nextState);
        return true;
    }
    componentWillUpdate() {
        console.log('Will component update?');
        return true;
    }
    componentDidUpdate(prevProps, prevState) {
        console.log('Did component update?');
        return true;
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1>Example getting data from Github API</h1>
                    </div>
                    <div className="col-md-12">
                        <form onSubmit={this.loadUserData}>
                            <div className="input-group">
                                <input className="form-control" type="text" value={this.state.search.userName} onChange={this.onInputChange}/>
                                <div className="input-group-btn">
                                    <button type="submit" className="btn btn-success">Load data for this user</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br/>
                    {!this.state.isFetched ? <Message message="Fetching data from github"/> : null}
                    {this.state.isFetched ? <UserCard user={this.state.user}/> : null}
                </div>
            </div>
        );
    }
}

export default HomePage;