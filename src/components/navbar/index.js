import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  NavLink
} from 'react-router-dom';
import Home from '../home';
import About from '../about';
import ES6Stateless from '../stateless/es6';

const Navbar = () => (
  <Router>
    <div>
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="collapse navbar-collapse">
            <ul className="nav navbar-nav">
              <li><NavLink to="/">Home</NavLink></li>
              <li><NavLink to="/about">About</NavLink></li>
              <li><NavLink to="/stateless">Stateless</NavLink></li>
            </ul>
          </div>
        </div>
      </nav>
      <Route exact path="/" component={Home}/>
      <Route path="/about" component={About}/>
      <Route path="/stateless" component={ES6Stateless}/>
    </div>
  </Router>
)

export default Navbar;